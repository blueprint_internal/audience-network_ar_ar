﻿1
00:00:03,134 --> 00:00:06,888
Facebook Reklamlarıyla hikayenizi paylaşın ve işletmenizi büyütün.

2
00:00:08,500 --> 00:00:12,130
Facebook doğru insanları bulmayı, onların dikkatini çekmeyi ve

3
00:00:12,130 --> 00:00:13,897
sonuçlara ulaşmayı kolaylaştırır.

4
00:00:13,897 --> 00:00:17,090
Artık reklamınızı Facebook dışında da paylaşabilirsiniz.

5
00:00:17,090 --> 00:00:19,964
Audience Network, mobil uygulamalar ve internet sitelerinde daha fazla

6
00:00:19,964 --> 00:00:20,927
insana erişmenize yardımcı olur.

7
00:00:22,253 --> 00:00:25,384
Bildiğiniz ve sevdiğiniz Facebook hedefleme araçlarını kullanırsınız ve

8
00:00:25,384 --> 00:00:27,613
reklamınız ideal müşterilerinize,

9
00:00:27,613 --> 00:00:28,728
zamanlarını geçirdikleri yerde erişir.

10
00:00:29,974 --> 00:00:33,220
Mobil uygulamaların ve internet sitelerinin hepsi kalite açısından incelenir.

11
00:00:33,220 --> 00:00:37,725
Hassas kategori veya siteleri hariç tutma olanağınız da ardır.

12
00:00:37,725 --> 00:00:39,440
Kampanyanızın reklam verme amacına bağlı olarak,

13
00:00:39,440 --> 00:00:41,532
elinizdeki bütçeyle sonuçlarınızı en üst düzeye çıkarmak

14
00:00:41,532 --> 00:00:44,450
için reklamınız farklı reklam alanlarında gösterilir.

15
00:00:44,450 --> 00:00:46,347
Özellik şu şekilde çalışır.

16
00:00:46,347 --> 00:00:48,525
Müşterilerinize erişmeye yönelik her fırsat için,

17
00:00:48,525 --> 00:00:50,570
sonuç başına farklı bir ücret bulunur.

18
00:00:50,570 --> 00:00:52,536
Yalnızca Facebook'ta reklam verdiğinizde

19
00:00:52,536 --> 00:00:56,717
reklamınız, bütçeniz dahilinde mümkün olan en fazla sonucu sağlar.

20
00:00:56,717 --> 00:01:00,216
Bu örnekte, 10 $ bütçe ile dört sonuç elde edersiniz.

21
00:01:01,474 --> 00:01:03,187
Audience Network'ü ekleyerek,

22
00:01:03,187 --> 00:01:05,659
reklamınızı daha fazla yerde gösterme şansı elde edebilirsiniz.

23
00:01:05,659 --> 00:01:08,321
Öncelikle daha düşük ücretli reklam alanları doldurularak

24
00:01:08,321 --> 00:01:11,436
aynı bütçeyle daha fazla sonuç almanız sağlanır.

25
00:01:11,436 --> 00:01:15,150
Örneğimizdeki gibi bire bir aynı kampanyalar oluşturun

26
00:01:15,150 --> 00:01:17,879
ve bunları aynı anda yayınlayın.

27
00:01:17,879 --> 00:01:21,305
Facebook ve Audience Network'ün birlikte

28
00:01:21,305 --> 00:01:24,481
bütçeniz dahilindeki en iyi sonuçları sağladığını göreceksiniz.

29
00:01:24,481 --> 00:01:28,160
Audience Network'ün, kampanya reklam alanı ayarlarına dahil olduğundan emin olarak

30
00:01:28,160 --> 00:01:30,456
reklam bütçenizden alacağınız verimi artırın.

